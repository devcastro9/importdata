﻿Imports System.Data.SqlClient
Public Class DAO
    Inherits ConnectionDB
    Public Async Function ExecutingNonQuery(commandText As String) As Task(Of Integer)
        Using conn = GetConnection()
            Await conn.OpenAsync()
            Using cmd = New SqlCommand(commandText, conn)
                Return Await cmd.ExecuteNonQueryAsync()
            End Using
        End Using
    End Function

    Public Async Function ExecutingEscalar(commandText As String) As Task(Of Object)
        Using conn = GetConnection()
            Await conn.OpenAsync()
            Using cmd = New SqlCommand(commandText, conn)
                Return Await cmd.ExecuteScalarAsync()
            End Using
        End Using
    End Function

    Public Async Function ExecutingReader(commandText As String) As Task(Of SqlDataReader)
        Using conn = GetConnection()
            Await conn.OpenAsync()
            Using cmd = New SqlCommand(commandText, conn)
                Return Await cmd.ExecuteReaderAsync()
                'Return Await cmd.ExecuteReaderAsync(CommandBehavior.CloseConnection)
            End Using
        End Using
    End Function
End Class
