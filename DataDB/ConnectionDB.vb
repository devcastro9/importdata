﻿Imports System.Data.SqlClient

Public MustInherit Class ConnectionDB
    Private connectionString As String

    Protected Sub New()
        connectionString = "Server=sssofia;DataBase=ADMIN_EMPRESA;Integrated Security=True"
    End Sub

    Protected Function GetConnection() As SqlConnection
        Return New SqlConnection(connectionString)
    End Function
End Class
