﻿Imports DataDB
Imports System.Data.SqlClient
Public Class UserModel
    Dim uDAO As New DAO
    Public Shared Function Decrypt(cadena As String) As String
        Dim i, j As Byte
        Dim encrypt As String = ""
        For i = 1 To Len(cadena)
            j = Asc(Mid(cadena, i, 1)) + 5
            encrypt += Chr(j)
        Next
        Return encrypt
    End Function
    Public Async Function Login(user As String, pass As String) As Task(Of Boolean)
        Dim query As String = "SELECT COUNT(*) FROM dbo.gc_usuarios "
        query += "WHERE usr_codigo = '" & user & "' AND usr_clave = '" & Decrypt(pass) & "'"
        Dim count As Integer = Await uDAO.ExecutingEscalar(query)
        If count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Async Function ChargeMes(gestion As Integer) As Task(Of DataTable)
        Dim query As String = "SELECT mes, mes_literal FROM fo_extracto_mes_gestion WHERE gestion = " & gestion
        Dim reader As SqlDataReader
        Dim result As New DataTable
        reader = Await uDAO.ExecutingReader(query)
        result.Load(reader)
        Await reader.CloseAsync()
        Return result
    End Function

    Public Async Function ChargeGestion() As Task(Of DataTable)
        Dim query As String = "select gestion from dbo.fo_extracto_mes_gestion"
        Dim reader As SqlDataReader
        Dim result As New DataTable
        reader = Await uDAO.ExecutingReader(query)
        result.Load(reader)
        Await reader.CloseAsync()
        Return result
    End Function

    Public Async Function gestion() As Task(Of SqlDataReader)
        Dim query As String = "select gestion from dbo.fo_extracto_mes_gestion"
        Return Await uDAO.ExecutingReader(query)
    End Function
End Class
