﻿Imports System.Data.SqlClient
Imports Domain

Public Class frmLogin
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Application.Exit()
    End Sub

    Private Sub frmLogin_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtUser.Select()
    End Sub

    Private Async Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        Dim oUser As New UserModel()
        Dim auth As Boolean = False
        auth = Await oUser.Login(txtUser.Text, txtPassword.Text)
        If auth Then
            frmMain.Show()
            Me.Hide()
        Else
            'MsgBox("Usuario o contraseña erronea.", MsgBoxStyle.Critical, "ERROR")
            Dim leer As SqlDataReader
            leer = Await oUser.gestion()
            While leer.Read()
                MsgBox(leer(0).ToString())
            End While
        End If
    End Sub
End Class
