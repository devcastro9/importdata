﻿Public Class frmMain
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Application.Exit()
    End Sub

    Private Sub AbrirFormEnPanel(Of frm As {Form, New})()
        Dim formulario As Form = pnlContenedor.Controls.OfType(Of frm)().FirstOrDefault()
        If formulario Is Nothing Then
            formulario = New frm
            formulario.TopLevel = False
            formulario.FormBorderStyle = FormBorderStyle.None
            formulario.Dock = DockStyle.Fill
            pnlContenedor.Controls.Add(formulario)
            pnlContenedor.Tag = formulario
            formulario.Show()
        Else
            formulario.BringToFront()
        End If
    End Sub

    Private Sub btnExtracto_Click(sender As Object, e As EventArgs) Handles btnExtracto.Click
        AbrirFormEnPanel(Of frmExtracto)()
    End Sub
End Class