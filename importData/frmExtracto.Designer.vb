﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExtracto
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.cboMes = New System.Windows.Forms.ComboBox()
        Me.lbMes = New System.Windows.Forms.Label()
        Me.lblGetion = New System.Windows.Forms.Label()
        Me.cboGestion = New System.Windows.Forms.ComboBox()
        Me.btnCargar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnCerrar
        '
        Me.btnCerrar.BackColor = System.Drawing.Color.Maroon
        Me.btnCerrar.FlatAppearance.BorderSize = 0
        Me.btnCerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCerrar.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
        Me.btnCerrar.ForeColor = System.Drawing.Color.White
        Me.btnCerrar.Location = New System.Drawing.Point(658, 11)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(30, 30)
        Me.btnCerrar.TabIndex = 0
        Me.btnCerrar.Text = "X"
        Me.btnCerrar.UseVisualStyleBackColor = False
        '
        'cboMes
        '
        Me.cboMes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMes.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.cboMes.FormattingEnabled = True
        Me.cboMes.Location = New System.Drawing.Point(438, 107)
        Me.cboMes.Name = "cboMes"
        Me.cboMes.Size = New System.Drawing.Size(142, 29)
        Me.cboMes.TabIndex = 1
        '
        'lbMes
        '
        Me.lbMes.AutoSize = True
        Me.lbMes.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lbMes.Location = New System.Drawing.Point(488, 71)
        Me.lbMes.Name = "lbMes"
        Me.lbMes.Size = New System.Drawing.Size(39, 21)
        Me.lbMes.TabIndex = 2
        Me.lbMes.Text = "Mes"
        '
        'lblGetion
        '
        Me.lblGetion.AutoSize = True
        Me.lblGetion.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblGetion.Location = New System.Drawing.Point(143, 71)
        Me.lblGetion.Name = "lblGetion"
        Me.lblGetion.Size = New System.Drawing.Size(63, 21)
        Me.lblGetion.TabIndex = 4
        Me.lblGetion.Text = "Gestion"
        '
        'cboGestion
        '
        Me.cboGestion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGestion.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.cboGestion.FormattingEnabled = True
        Me.cboGestion.Location = New System.Drawing.Point(104, 107)
        Me.cboGestion.Name = "cboGestion"
        Me.cboGestion.Size = New System.Drawing.Size(142, 29)
        Me.cboGestion.TabIndex = 3
        '
        'btnCargar
        '
        Me.btnCargar.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnCargar.FlatAppearance.BorderSize = 0
        Me.btnCargar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCargar.Font = New System.Drawing.Font("Segoe UI Semibold", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
        Me.btnCargar.ForeColor = System.Drawing.Color.White
        Me.btnCargar.Location = New System.Drawing.Point(218, 176)
        Me.btnCargar.Name = "btnCargar"
        Me.btnCargar.Size = New System.Drawing.Size(247, 46)
        Me.btnCargar.TabIndex = 5
        Me.btnCargar.Text = "CARGAR"
        Me.btnCargar.UseVisualStyleBackColor = False
        '
        'frmExtracto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(700, 505)
        Me.Controls.Add(Me.btnCargar)
        Me.Controls.Add(Me.lblGetion)
        Me.Controls.Add(Me.cboGestion)
        Me.Controls.Add(Me.lbMes)
        Me.Controls.Add(Me.cboMes)
        Me.Controls.Add(Me.btnCerrar)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmExtracto"
        Me.Text = "frmExtracto"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnCerrar As Button
    Friend WithEvents cboMes As ComboBox
    Friend WithEvents lbMes As Label
    Friend WithEvents lblGetion As Label
    Friend WithEvents cboGestion As ComboBox
    Friend WithEvents btnCargar As Button
End Class
