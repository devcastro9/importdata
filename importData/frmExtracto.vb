﻿Imports Domain

Public Class frmExtracto
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub frmExtracto_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim mesActual As Integer = Month(Today)
        Dim gestionActual As Integer = Year(Today)
        Dim oCombo As New UserModel()
        'cboGestion.DataBindings = ""
        cboGestion.DataSource = oCombo.ChargeGestion()
        cboGestion.DisplayMember = "gestion"
        cboGestion.ValueMember = "gestion"
        cboGestion.SelectedIndex = gestionActual
        cboMes.DataSource = oCombo.ChargeMes(gestionActual)
        cboMes.DisplayMember = "mes_literal"
        cboMes.ValueMember = "mes"
        cboMes.SelectedIndex = mesActual
    End Sub
End Class